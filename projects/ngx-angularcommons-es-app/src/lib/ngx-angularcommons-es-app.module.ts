import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER } from '@angular/core';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';

import { CommonsConfigService } from './services/commons-config.service';
import { CommonsDelayedService } from './services/commons-delayed.service';
import { CommonsSettingsService } from './services/commons-settings.service';
import { CommonsLoadingService } from './services/commons-loading.service';

export function configLoader(config: CommonsConfigService): () => Promise<void> {
	const lambda: () => Promise<void> = async () => await config.load('assets/config/app-config.json');
	return lambda;
}

@NgModule({
		imports: [
				CommonModule,
				HttpClientModule,
				NgxAngularCommonsEsCoreModule
		],
		declarations: [
		],
		exports: [
		]
})
export class NgxAngularCommonsEsAppModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsEsAppModule> {
		return {
				ngModule: NgxAngularCommonsEsAppModule,
				providers: [
						CommonsConfigService,
						CommonsDelayedService,
						CommonsSettingsService,
						CommonsLoadingService,
						{
								provide: APP_INITIALIZER,
								useFactory: configLoader,
								deps: [CommonsConfigService],
								multi: true
						}
				]
		};
	}
}
