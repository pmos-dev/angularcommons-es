import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { firstValueFrom } from 'rxjs';

import {
		commonsTypeHasPropertyObject,
		commonsTypeHasProperty,
		commonsTypeAttemptObject,
		commonsTypeAttemptString,
		commonsTypeAttemptNumber,
		commonsTypeAttemptBoolean,
		TPropertyObject
} from 'tscommons-es-core';

@Injectable()
export class CommonsConfigService {
	private config: TPropertyObject = {};

	constructor(
			private http: HttpClient
	) {}

	public async load(configPath: string): Promise<void> {
		try {
			this.config = await firstValueFrom(this.http.get<TPropertyObject>(configPath));
		} catch (error) {
			console.log(error);
		}
	}

	public getDirect(context: string): TPropertyObject|undefined {
		if (!commonsTypeHasPropertyObject(this.config, context)) return undefined;
		return this.config[context];
	}

	public getAny(context: string, key: string, defaultValue?: any): any|undefined {
		if (
			!commonsTypeHasPropertyObject(this.config, context)
				|| !commonsTypeHasProperty(this.config[context], key)
		) return defaultValue;
		
		return this.config[context][key];
	}

	public getObject<T extends TPropertyObject>(context: string, key: string, defaultValue?: T): T|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return commonsTypeAttemptObject(value) as T;
	}

	public getString(context: string, key: string, defaultValue?: string): string|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return commonsTypeAttemptString(value);
	}
	
	public getNumber(context: string, key: string, defaultValue?: number): number|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);

		return commonsTypeAttemptNumber(value);
	}

	public getBoolean(context: string, key: string, defaultValue?: boolean): boolean|undefined {
		const value: any|undefined = this.getAny(context, key, defaultValue);
		
		return commonsTypeAttemptBoolean(value);
	}
}
