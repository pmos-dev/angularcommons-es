import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { CommonsStorageService } from 'ngx-angularcommons-es-core';
import { ICommonsStored } from 'ngx-angularcommons-es-core';

import { CommonsConfigService } from './commons-config.service';

@Injectable()
export class CommonsSettingsService {
	private namespace: string;

	private modified: Subject<ICommonsStored> = new Subject<ICommonsStored>();

	constructor(
		private configService: CommonsConfigService,
		private storageService: CommonsStorageService
	) {
		this.namespace = this.configService.getString('storage', 'namespace', 'webapp') || 'webapp';
	
		this.storageService.observable()
			.subscribe((stored: ICommonsStored): void => {
				if (stored.key.substring(0, this.namespace.length + 1) !== `${this.namespace}_`) return;
					
				const subKey: string = stored.key.substring(this.namespace.length + 1);
					
				this.modified.next({
						key: subKey,
						value: stored.value
				});
			});
	}

	public getAny(store: Storage|string, key: string, defaultValue?: any): any|undefined {
		return this.storageService.getAny(store, `${this.namespace}_${key}`, defaultValue);
	}

	public getString(store: Storage|string, key: string, defaultValue?: string): string|undefined {
		return this.storageService.getString(store, `${this.namespace}_${key}`, defaultValue);
	}
		
	public getNumber(store: Storage|string, key: string, defaultValue?: number): number|undefined {
		return this.storageService.getNumber(store, `${this.namespace}_${key}`, defaultValue);
	}

	public getBoolean(store: Storage|string, key: string, defaultValue?: boolean): boolean|undefined {
		return this.storageService.getBoolean(store, `${this.namespace}_${key}`, defaultValue);
	}

	public delete(store: Storage|string, key: string, supress?: boolean): void {
		return this.storageService.delete(store, `${this.namespace}_${key}`, supress);
	}

	public setAny(store: Storage|string, key: string, value: any): void {
		this.storageService.setAny(store, `${this.namespace}_${key}`, value);
	}

	public setString(store: Storage|string, key: string, value: string): void {
		this.storageService.setString(store, `${this.namespace}_${key}`, value);
	}
		
	public setNumber(store: Storage|string, key: string, value: number): void {
		this.storageService.setNumber(store, `${this.namespace}_${key}`, value);
	}
	
	public setBoolean(store: Storage|string, key: string, value: boolean): void {
		this.storageService.setBoolean(store, `${this.namespace}_${key}`, value);
	}

	public reset(store: Storage|string, supress?: boolean): void {
		for (const key of Object.keys(store)) {
			if (key.substr(0, this.namespace.length + 1) === `${this.namespace}_`) {
				this.storageService.delete(store, key, supress);
			}
		}
	}
	
	public observable(): Observable<ICommonsStored> {
		return this.modified.asObservable();
	}

}
