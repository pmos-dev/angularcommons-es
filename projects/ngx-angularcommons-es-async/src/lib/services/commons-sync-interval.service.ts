import { Injectable, EventEmitter } from '@angular/core';

import { Observable, Subscription, timer } from 'rxjs';

type TInterval = {
		emitter: EventEmitter<number>;
		tally: number;
		subscription: Subscription;
		isPaused: boolean;
};

@Injectable({
		providedIn: 'root'
})
export class CommonsSyncIntervalService {
	private intervals: Map<string, TInterval> = new Map<string, TInterval>();

	public add(id: string, interval: number, paused: boolean = false): boolean {
		if (this.intervals.has(id)) return false;
		
		const subscription: Subscription = timer(
				interval,
				interval
		)
			.subscribe((): void => {
				const i: TInterval|undefined = this.intervals.get(id);
				if (i === undefined) return;
				if (i.isPaused) return;
					
				i.emitter.emit(i.tally);

				i.tally++;
			});
		
		this.intervals.set(id, {
				emitter: new EventEmitter<number>(true),
				tally: 0,
				subscription: subscription,
				isPaused: paused
		});
		
		return true;
	}
	
	public intervalListener(id: string): Observable<number>|undefined {
		const interval: TInterval|undefined = this.intervals.get(id);
		
		if (interval === undefined) return undefined;
		return interval.emitter;
	}
	
	public pause(id: string): boolean {
		const interval: TInterval|undefined = this.intervals.get(id);
		if (interval === undefined) return false;
		
		interval.isPaused = true;
		return true;
	}
	
	public resume(id: string): boolean {
		const interval: TInterval|undefined = this.intervals.get(id);
		if (interval === undefined) return false;
		
		interval.isPaused = false;
		return true;
	}

	public remove(id: string): boolean {
		const interval: TInterval|undefined = this.intervals.get(id);
		if (interval === undefined) return false;
		
		interval.subscription.unsubscribe();
		this.intervals.delete(id);
		
		return true;
	}
}
