import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
		providedIn: 'root'
})
export class CommonsFocusService {
	private onElementFocus: EventEmitter<string> = new EventEmitter<string>(true);
	
	public elementFocusObservable(): Observable<string> {
		return this.onElementFocus;
	}
	
	public elementFocus(id: string): void {
		this.onElementFocus.emit(id);
	}
}
