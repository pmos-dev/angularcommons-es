import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
		providedIn: 'root'
})
export class CommonsVisibilityService {
	private onVisibilityChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

	private hidden: string|undefined;

	constructor() {
		let visibilityChange: string|undefined;
		if (document.hidden !== undefined) {
			this.hidden = 'hidden';
			visibilityChange = 'visibilitychange';
		} else if ((document as any).msHidden !== undefined) {
			this.hidden = 'msHidden';
			visibilityChange = 'msvisibilitychange';
		} else if ((document as any).webkitHidden !== undefined) {
			this.hidden = 'webkitHidden';
			visibilityChange = 'webkitvisibilitychange';
		} else if ((document as any).mozHidden !== undefined) {
			this.hidden = 'mozHidden';
			visibilityChange = 'mozvisibilitychange';
		}

		if (!this.isSupported()) return;

		document.addEventListener(
				visibilityChange!,
				(): void => {
					this.onVisibilityChange.emit(this.isVisible());
				},
				false
		);
	}

	public isSupported(): boolean {
		return this.hidden !== undefined;
	}

	public isVisible(): boolean {
		if (!this.isSupported()) throw new Error('PageVisibility API is not supported');
		return (document as any)[this.hidden!] !== true;
	}

	public visibilityChangeObservable(): Observable<boolean> {
		return this.onVisibilityChange;
	}
}
