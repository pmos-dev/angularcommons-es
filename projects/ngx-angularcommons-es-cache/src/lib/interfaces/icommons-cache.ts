import { TEncoded } from 'tscommons-es-core';

export interface ICommonsCache {
		value: TEncoded;
		timestamp: number;
		expiry: number;
}
