import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';

import { CommonsCacheService } from './services/commons-cache.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule
		],
		declarations: [],
		exports: []
})
export class NgxAngularCommonsEsCacheModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsEsCacheModule> {
		return {
				ngModule: NgxAngularCommonsEsCacheModule,
				providers: [
						CommonsCacheService
				]
		};
	}
}
