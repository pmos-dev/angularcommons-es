import { OnInit, OnDestroy, Directive } from '@angular/core';
import { NgZone, ChangeDetectorRef } from '@angular/core';

import { Subscription, Observable } from 'rxjs';

import { CommonsComponent } from './commons.component';

@Directive()
export abstract class CommonsManualChangeDetectionComponent extends CommonsComponent implements OnInit, OnDestroy {
	private internalCommonsIntervals: any[] = [];
	
	private isComponentDestroyed: boolean = false;
	
	constructor(
			private zone: NgZone,
			private changeDetector: ChangeDetectorRef
	) {
		super();
		
		this.isComponentDestroyed = false;
	}

	override ngOnInit(): void {
		super.ngOnInit();
		
		this.isComponentDestroyed = false;

		// run change detection once, then detach
		this.runChangeDetection();
	}

	override ngOnDestroy(): void {
		for (const thread of this.internalCommonsIntervals) {
			window.clearInterval(thread);
		}
		
		this.isComponentDestroyed = true;
		
		super.ngOnDestroy();
	}

	protected runDetached(code: () => void): void {
		this.zone.runOutsideAngular((): void => {
			code();
		});
	}

	protected override subscribe(
			observable: Observable<any>,
			handler: (...args: unknown[]) => void
	): Subscription {
		const wrapper: (...args: unknown[]) => void = (...args: unknown[]) => {
			this.runDetached((): void => {
				handler.apply(null, args);
			});
		};

		return super.subscribe(observable, wrapper);
	}
	
	protected setDetachedTimeout(start: number, callback: () => void): void {
		this.runDetached((): void => {
			window.setTimeout(
					(): void => {
						this.runDetached((): void => {
							callback();
						});
					},
					start
			);
		});
	}
	
	protected setDetachedInterval(start: number, interval: number, callback: () => void): void {
		this.setDetachedTimeout(
				start,
				(): void => {
					if (this.isComponentDestroyed) return;
								
					callback();

					const thread: any = window.setInterval(
							(): void => {
								if (this.isComponentDestroyed) return;
								
								this.runDetached((): void => {
									callback();
								});
							},
							interval
					);
					
					this.internalCommonsIntervals.push(thread);
				}
		);
	}
	
	protected runChangeDetection(): void {
		if (this.isComponentDestroyed) return;
		
		this.changeDetector.reattach();
		this.changeDetector.detectChanges();
		this.changeDetector.detach();
	}
}
