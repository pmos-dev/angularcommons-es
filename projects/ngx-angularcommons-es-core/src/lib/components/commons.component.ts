import { OnInit, AfterViewInit, OnDestroy, Directive } from '@angular/core';

import { Observable, Subscription, timer } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { commonsBase62GenerateRandomId } from 'tscommons-es-core';
import { ECommonsInvocation } from 'tscommons-es-async';

@Directive()
export abstract class CommonsComponent implements OnInit, AfterViewInit, OnDestroy {
	private internalCommonsSubscriptions: Subscription[] = [];
	private internalCommonsIsInCycle: Map<string, boolean> = new Map<string, boolean>();
	//? private internalCommonsIsAborted: Map<string,boolean> = new Map<string,boolean>();

	private internalCommonsIsDestroyed: boolean = true;
	
	ngOnInit(): void {
		this.ngOnDestroy();	// clean up any hanging from previous ngOnInit();
		
		this.internalCommonsIsDestroyed = false;
	}

	ngAfterViewInit(): void {
		// placeholder block, just to allow for super calls in subclasses
	}

	ngOnDestroy(): void {
		this.internalCommonsIsDestroyed = true;
		
		for (const subscription of this.internalCommonsSubscriptions) {
			if (subscription) subscription.unsubscribe();
		}
		this.internalCommonsSubscriptions = [];
	}
	
	protected subscribe(
			observable: Observable<any>,
			handler: (_: any) => void
	): Subscription {
		const subscription: Subscription = observable.subscribe(handler);
		
		this.internalCommonsSubscriptions.push(subscription);
		
		return subscription;
	}
	
	private setTimer(start: number, interval: number|undefined, callback: () => Promise<void>): Subscription {
		const subscription: Subscription = this.subscribe(
				interval !== undefined ? timer(start, interval) : timer(start),
				async (): Promise<void> => {
					if (interval === undefined) {
						const closure: Subscription|undefined = this.internalCommonsSubscriptions
							.find((s: Subscription): boolean => s === subscription);
	
						if (closure) {
							const index: number = this.internalCommonsSubscriptions.indexOf(closure);
	
							closure.unsubscribe();
							this.internalCommonsSubscriptions.splice(index, 1);
						}
					}
					
					await callback();
				}
		);
		
		return subscription;
	}

	protected setInterval(
			start: number,
			interval: number,
			approach: ECommonsInvocation,
			callback: () => Promise<void>
	): Subscription {
		switch (approach) {
		case ECommonsInvocation.FIXED: {
			return this.setTimer(start, interval, callback);
		}
		case ECommonsInvocation.WHEN: {
			// This approach may lead to a stack overflow due to the deep nesting
				
			let cycle: (() => Promise<void>)|undefined;
			cycle = async (): Promise<void> => {
				if (this.internalCommonsIsDestroyed || !cycle) return;
					
				try {
					await callback();
				} catch (ex) {
					// this may not be necessary, but not sure if the finally runs without a catch clause
					throw ex;
				} finally {
					if (cycle) this.setTimeout(interval, cycle);
				}
			};

			const monitor: Subscription = this.subscribe(
					timer(1000, 1000)
						.pipe(
								finalize((): void => {
									// when unsubscribed, abort the loop
									cycle = undefined;
								})
						),
					(): void => {
						// do nothing
					}
			);
				
			this.setTimeout(start, cycle);

			return monitor;
		}
		case ECommonsInvocation.IF: {
			const internalId = commonsBase62GenerateRandomId();
				
			this.internalCommonsIsInCycle.set(internalId, false);
				
			return this.setInterval(
					start,
					interval,
					ECommonsInvocation.FIXED,
					async (): Promise<void> => {
						if (this.internalCommonsIsInCycle.get(internalId)) return;

						try {
							this.internalCommonsIsInCycle.set(internalId, true);
							await callback();
						} finally {
							this.internalCommonsIsInCycle.set(internalId, false);
						}
					}
			);
		}
		}
		
		throw new Error('Unknown interval approach');
	}

	protected setTimeout(ms: number, callback: () => Promise<void>): Subscription {
		return this.setTimer(ms, undefined, callback);
	}
}
