import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsStatusService } from './services/commons-status.service';
import { CommonsStorageService } from './services/commons-storage.service';
import { CommonsMultiPromiseCompletionService } from './services/commons-multi-promise-completion.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [],
		exports: []
})
export class NgxAngularCommonsEsCoreModule {
	static forRoot(): ModuleWithProviders<NgxAngularCommonsEsCoreModule> {
		return {
				ngModule: NgxAngularCommonsEsCoreModule,
				providers: [
						CommonsStatusService,
						CommonsStorageService,
						CommonsMultiPromiseCompletionService
				]
		};
	}
}
