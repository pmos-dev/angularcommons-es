import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class CommonsStatusService {
	private behaviours: Map<string, BehaviorSubject<any>> = new Map<string, BehaviorSubject<any>>();
	
	public setup<T = any>(name: string, defaultValue: T): void {
		if (!this.behaviours.has(name)) {
			this.behaviours.set(name, new BehaviorSubject<T>(defaultValue));
		}
	}

	public observable<T = any>(name: string, defaultValue: T): Observable<T> {
		this.setup<T>(name, defaultValue);
		
		const typecast: BehaviorSubject<T> = (this.behaviours.get(name)!) as BehaviorSubject<T>;
		return typecast.asObservable();
	}

	public setStatus<T = any>(name: string, value: T): void {
		this.setup<T>(name, value);
		
		const typecast: BehaviorSubject<T> = (this.behaviours.get(name)!) as BehaviorSubject<T>;
		typecast.next(value);
	}
}
