import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
		commonsTypeAttemptString,
		commonsTypeAttemptNumber,
		commonsTypeAttemptBoolean
} from 'tscommons-es-core';

import { ICommonsStored } from '../interfaces/icommons-stored';

@Injectable()
export class CommonsStorageService {
	public static SESSION: Storage = sessionStorage;
	public static LOCAL: Storage = localStorage;

	private modified: Subject<ICommonsStored> = new Subject<ICommonsStored>();

	private useStore(store: Storage|string): Storage {
		if (typeof store === 'string') {
			switch (store.toLowerCase()) {
			case 'session':
				return CommonsStorageService.SESSION;
			case 'local':
				return CommonsStorageService.LOCAL;
			default:
				throw new Error(`Unknown storage medium ${store}`);
			}
		}
		
		return store as Storage;
	}
	
	public getAny(store: Storage|string, key: string, defaultValue?: any): any|undefined {
		const value: string|undefined|null = this.useStore(store).getItem(key);
		if (value !== undefined && value !== null) return JSON.parse(value);
		
		return defaultValue;
	}

	public getString(store: Storage|string, key: string, defaultValue?: string): string|undefined {
		const value: any|undefined = this.getAny(store, key, defaultValue);

		return commonsTypeAttemptString(value);
	}
		
	public getNumber(store: Storage|string, key: string, defaultValue?: number): number|undefined {
		const value: any|undefined = this.getAny(store, key, defaultValue);

		return commonsTypeAttemptNumber(value);
	}

	public getBoolean(store: Storage|string, key: string, defaultValue?: boolean): boolean|undefined {
		const value: any|undefined = this.getAny(store, key, defaultValue);
		
		return commonsTypeAttemptBoolean(value);
	}

	public delete(store: Storage|string, key: string, supress: boolean = false): void {
		this.useStore(store).removeItem(key);

		if (!supress) {
			this.modified.next({
					key: key,
					value: undefined
			});
		}
	}

	public setAny(store: Storage|string, key: string, value: any): void {
		this.useStore(store).setItem(key, JSON.stringify(value));

		this.modified.next({
				key: key,
				value: value
		});
	}

	public setString(store: Storage|string, key: string, value: string): void {
		this.setAny(store, key, value);
	}
		
	public setNumber(store: Storage|string, key: string, value: number): void {
		this.setAny(store, key, value);
	}
	
	public setBoolean(store: Storage|string, key: string, value: boolean): void {
		this.setAny(store, key, value);
	}
	
	public observable(): Observable<ICommonsStored> {
		return this.modified.asObservable();
	}
}
