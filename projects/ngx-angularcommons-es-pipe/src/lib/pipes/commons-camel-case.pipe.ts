import { Pipe, PipeTransform } from '@angular/core';

import { commonsStringCamelCase } from 'tscommons-es-core';

@Pipe({
		name: 'commonsCamelCase'
})
export class CommonsCamelCasePipe implements PipeTransform {

	transform(value: string|any, enabled: boolean = true, ucFirst: boolean = true): any {
		if (!enabled) return value;
		
		if (value === undefined) return '';

		return commonsStringCamelCase(value, ucFirst);
	}

}
