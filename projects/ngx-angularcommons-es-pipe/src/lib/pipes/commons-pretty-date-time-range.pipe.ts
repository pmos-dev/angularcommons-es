import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsDateArray, commonsDatePrettyDateTimeRange } from 'tscommons-es-core';

// This pipe is odd in that it needs two variables, so uses an array as input

@Pipe({
		name: 'commonsPrettyDateTimeRange'
})
export class CommonsPrettyDateTimeRangePipe implements PipeTransform {
	transform(
			values: Date[]|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			includeYear?: boolean,
			iso?: boolean
	): string|Date[] {
		if (!commonsTypeIsDateArray(values)) return '-';

		if (!enabled) return values;

		return commonsDatePrettyDateTimeRange(
				{
						from: values[0],
						to: values[1]
				},
				absolute,
				now,
				includeYear,
				iso
		);
	}

}
