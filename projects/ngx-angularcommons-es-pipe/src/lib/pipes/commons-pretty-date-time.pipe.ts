import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsDate, commonsDatePrettyDateTime } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyDateTime'
})
export class CommonsPrettyDateTimePipe implements PipeTransform {

	transform(
			value: Date|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			includeYear?: boolean,
			iso?: boolean
	): string|Date {
		if (!commonsTypeIsDate(value)) return '-';

		if (!enabled) return value;

		return commonsDatePrettyDateTime(
				value,
				absolute,
				now,
				includeYear,
				iso
		);
	}

}
