import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsDate, commonsDatePrettyDate } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyDate'
})
export class CommonsPrettyDatePipe implements PipeTransform {

	transform(
			value: Date|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			includeYear?: boolean,
			iso?: boolean
	): string|Date {
		if (!commonsTypeIsDate(value)) return '-';

		if (!enabled) return value;
		
		return commonsDatePrettyDate(
				value,
				absolute,
				now,
				includeYear,
				iso
		);
	}

}
