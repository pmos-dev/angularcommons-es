import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsNumber, commonsNumberPrettyDuration } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyDuration'
})
export class CommonsPrettyDurationPipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, short: boolean = false): string|any {
		if (!enabled) return value;
		
		if (!commonsTypeIsNumber(value)) return '-';
		
		return commonsNumberPrettyDuration(value, short);
	}

}
