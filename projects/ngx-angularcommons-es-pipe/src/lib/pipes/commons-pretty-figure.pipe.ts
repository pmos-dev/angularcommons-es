import { Pipe, PipeTransform } from '@angular/core';

import { commonsNumberPrettyFigure } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFigure'
})
export class CommonsPrettyFigurePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, absolute: boolean = false): any {
		if (!enabled) return value;
		
		if (value === undefined) return '-';

		return commonsNumberPrettyFigure(value!, absolute);
	}

}
