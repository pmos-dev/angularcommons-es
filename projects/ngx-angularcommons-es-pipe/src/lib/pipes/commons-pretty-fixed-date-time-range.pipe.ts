import { Pipe, PipeTransform } from '@angular/core';

import { CommonsFixedDateRange } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFixedDateTimeRange'
})
export class CommonsPrettyFixedDateTimeRangePipe implements PipeTransform {
	transform(
			values: CommonsFixedDateRange|undefined,
			includeYear: boolean = true,
			short: boolean = false
	): string {
		if (!values) return '-';

		if (short && includeYear) return values.prettyShort;
		if (short && !includeYear) return values.prettyShortWithoutYear;
		if (!short && includeYear) return values.pretty;
		if (!short && !includeYear) return values.prettyWithoutYear;

		return '-';
	}

}
