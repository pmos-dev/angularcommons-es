import { Pipe, PipeTransform } from '@angular/core';

import { CommonsFixedDate } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFixedDateTime'
})
export class CommonsPrettyFixedDateTimePipe implements PipeTransform {

	transform(
			value: CommonsFixedDate|undefined,
			includeYear: boolean = true,
			short: boolean = false
	): string {
		if (!CommonsFixedDate.is(value)) return '-';

		if (short && includeYear) return `${value.prettyShort} ${value.Hi}`;
		if (short && !includeYear) return `${value.prettyShortWithoutYear} ${value.Hi}`;
		if (!short && includeYear) return `${value.pretty} ${value.Hi}`;
		if (!short && !includeYear) return `${value.prettyWithoutYear} ${value.Hi}`;

		return '-';
	}

}
