import { Pipe, PipeTransform } from '@angular/core';

import { CommonsFixedDate } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFixedDate'
})
export class CommonsPrettyFixedDatePipe implements PipeTransform {

	transform(
			value: CommonsFixedDate|undefined,
			includeYear: boolean = true,
			short: boolean = false
	): string {
		if (!CommonsFixedDate.is(value)) return '-';

		if (short && includeYear) return value.prettyShort;
		if (short && !includeYear) return value.prettyShortWithoutYear;
		if (!short && includeYear) return value.pretty;
		if (!short && !includeYear) return value.prettyWithoutYear;

		return '-';
	}

}
