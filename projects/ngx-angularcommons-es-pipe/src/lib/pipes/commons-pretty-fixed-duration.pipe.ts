import { Pipe, PipeTransform } from '@angular/core';

import { CommonsFixedDuration } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFixedDuration'
})
export class CommonsPrettyFixedDurationPipe implements PipeTransform {

	transform(value: CommonsFixedDuration|undefined): string|any {
		if (!value || !CommonsFixedDuration.is(value)) return '-';
		
		return value.pretty;
	}

}
