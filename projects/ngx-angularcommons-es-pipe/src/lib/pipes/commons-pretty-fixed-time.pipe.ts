import { Pipe, PipeTransform } from '@angular/core';

import { CommonsFixedDate } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyFixedTime'
})
export class CommonsPrettyFixedTimePipe implements PipeTransform {

	transform(
			value: CommonsFixedDate|undefined
	): string {
		if (!CommonsFixedDate.is(value)) return '-';

		return value.Hi;
	}

}
