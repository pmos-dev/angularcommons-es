import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
		name: 'commonsPrettyFloat'
})
export class CommonsPrettyFloatPipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true): string|number|any {
		if (!enabled) return value;

		if (isNaN(value)) return '-';
		
		if (Math.floor(value) === Math.ceil(value)) return value.toString();
		
		if (value >= 100) return value.toFixed(1);
		
		return value.toPrecision(3);
	}

}
