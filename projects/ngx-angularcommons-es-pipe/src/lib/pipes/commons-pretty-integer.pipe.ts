import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
		name: 'commonsPrettyInteger'
})
export class CommonsPrettyIntegerPipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true): string|number|any {
		if (!enabled) return value;
		
		if (isNaN(value)) return '-';
		
		return Math.floor(value).toLocaleString();
	}

}
