import { Pipe, PipeTransform } from '@angular/core';

import { commonsStringLimitLength } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyLimitLength'
})
export class CommonsPrettyLimitLengthPipe implements PipeTransform {

	transform(value: string|any, enabled: boolean = true, length: number = 128, soft: boolean = false): string|any {
		if (!enabled) return value;
		
		return commonsStringLimitLength(value, length, soft);
	}

}
