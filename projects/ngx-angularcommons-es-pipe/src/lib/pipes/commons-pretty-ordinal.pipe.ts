import { Pipe, PipeTransform } from '@angular/core';

import { commonsNumberPrettyOrdinal } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyOrdinal'
})
export class CommonsPrettyOrdinalPipe implements PipeTransform {
	transform(value: number|any, enabled: boolean = true): string|number|any {
		if (!enabled) return value;
		
		if (isNaN(value)) return '-';

		return commonsNumberPrettyOrdinal(value);
	}

}
