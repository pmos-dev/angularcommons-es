import { Pipe, PipeTransform } from '@angular/core';

import { commonsNumberPrettyPercent } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyPercentage'
})
export class CommonsPrettyPercentagePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, whole: boolean = false): string|number|any {
		if (!enabled) return value;
		
		if (isNaN(value)) return '-';

		return commonsNumberPrettyPercent(value, whole);
	}

}
