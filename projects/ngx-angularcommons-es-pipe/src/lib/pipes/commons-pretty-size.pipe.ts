import { Pipe, PipeTransform } from '@angular/core';

import { commonsNumberPrettyFileSize } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettySize'
})
export class CommonsPrettySizePipe implements PipeTransform {

	transform(value: number|any, enabled: boolean = true, binaryPrefix: boolean = false): any {
		if (!enabled) return value;
		
		if (value === undefined) return '-';

		return commonsNumberPrettyFileSize(value!, binaryPrefix);
	}

}
