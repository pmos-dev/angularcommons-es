import { Pipe, PipeTransform } from '@angular/core';

import { commonsTypeIsDate, commonsDatePrettyTime } from 'tscommons-es-core';

@Pipe({
		name: 'commonsPrettyTime'
})
export class CommonsPrettyTimePipe implements PipeTransform {

	transform(
			value: Date|unknown,
			enabled: boolean = true,
			absolute?: boolean,
			now?: Date,
			iso?: boolean
	): string|Date|any {
		if (!commonsTypeIsDate(value)) return '-';

		if (!enabled) return value;

		return commonsDatePrettyTime(
				value,
				absolute,
				now,
				iso
		);
	}
	
}
