import { Pipe, PipeTransform } from '@angular/core';

import { commonsStringUcWords } from 'tscommons-es-core';

@Pipe({
		name: 'commonsUcWords'
})
export class CommonsUcWordsPipe implements PipeTransform {

	transform(value: string|any, enabled: boolean = true): any {
		if (!enabled) return value;
		
		if (value === undefined) return '';

		return commonsStringUcWords(value);
	}

}
