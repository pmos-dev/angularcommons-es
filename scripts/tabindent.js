const fs = require('fs');

if (process.argv.length < 3) {
	console.error('No filename specified');
	process.exit(1);
}

const filepath = process.argv[2];

if (!fs.existsSync(filepath)) {
	console.error('No such file exists');
	process.exit(1);
}

let data = fs.readFileSync(filepath, 'utf8');

while (data.match(/\n[ ]{2}/)) {
	data = data.replace(/\n(.*)[ ]{2}([^ ])/g, '\n$1\t$2');
}

while (data.match(/[ \t]+\n/)) {
	data = data.replace(/[ \t]+\n/g, '\n');
}

fs.writeFileSync(filepath, data);

