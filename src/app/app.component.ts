import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'app-root',
		templateUrl: './app.component.html',
		styleUrls: ['./app.component.less']
})
export class AppComponent extends CommonsComponent implements OnInit {
	title = 'AngularCommonsEs';

	override ngOnInit(): void {
		super.ngOnInit();
		console.log('*');
	}
}
