import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';
import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';
import { NgxAngularCommonsEsAsyncModule } from 'ngx-angularcommons-es-async';
import { NgxAngularCommonsEsBrowserModule } from 'ngx-angularcommons-es-browser';
import { NgxAngularCommonsEsCacheModule } from 'ngx-angularcommons-es-cache';

import { AppComponent } from './app.component';

@NgModule({
		declarations: [
				AppComponent
		],
		imports: [
				BrowserModule,
				NgxAngularCommonsEsCoreModule.forRoot(),
				NgxAngularCommonsEsPipeModule,
				NgxAngularCommonsEsAppModule.forRoot(),
				NgxAngularCommonsEsAsyncModule.forRoot(),
				NgxAngularCommonsEsBrowserModule.forRoot(),
				NgxAngularCommonsEsCacheModule.forRoot()
		],
		providers: [],
		bootstrap: [AppComponent]
})
export class AppModule { }
